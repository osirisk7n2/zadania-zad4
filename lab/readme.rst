
Laboratorium #4
===============

1. Zdemonizować serwer HTTP z pracy domowej #2 i wyposażyć go w mechanizm logowania każdego żądania,
   odpowiedzi, czynności pośrednich (np. wyszukiwanie pliku) i ew. błędów.

2. Zbudować program do testowania wydajności serwera HTTP i przetestować nim swój serwer.
