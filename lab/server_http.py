# -*- coding: utf-8 -*-
__author__ = 'Ryujin'

import socket
import os
import email.utils
import logging
import time


logging.basicConfig(filename='example.log',level=logging.DEBUG)
log = logging.getLogger(__name__)
log.error('the system is down')

def http_serve(server_socket):


    while True:
        # Czekanie na polaczenie
        connection, client_address = server_socket.accept()

        try:
            # Odebranie zadania
            request = connection.recv(1024)
            if request:

                #print("Odebrano:")
                #print(request)
                #dobieramy sie do tego co chce od nas przegladarka
                request_method=str(request).split(' ')[0]
                request_protocol=str(request).split(' ')[2]
                request_uri=str(request).split(' ')[1]
                #page content and headers
                dir="web"
                naglowek_status="HTTP/1.1 400 Bad Request\r\n"
                naglowek_typTresci="Content-Type: text/html; charset=UTF-8\r\n"
                body=""

                #sprawdzamy czy to http i get, jak nie to zwracamy blad
                if 'HTTP' in request_protocol and "GET" in request_method:
                   # print("to jest http i GET!")
                    mod=True
                    #sprawdzamy czy sciezka w ogole istnieje, jak nie to 404
                    if os.path.exists(os.path.abspath(dir+request_uri)):
                        #naglowek_status="HTTP/1.1 200 OK\r\n"
                        #print(dir+request_uri)
                        #print(os.path.isdir(dir+request_uri))

                        #sprawdzamy czy to co uzytkownik podal to directory czy file, jak directory to zwracamy pliki i podkatalogi
                        if(os.path.isdir(dir+request_uri)):
                            mod=True
                            naglowek_status="HTTP/1.1 200 OK\r\n"
                            pliki=os.listdir(dir+request_uri)
                            #print(dir+request_uri)
                            rldir=dir+request_uri
                            for file in pliki:
                                #print(file)
                                #rozgraniczenie z ifami aby sciezka sie poprawnie formatowala (w dostepie do glebszych zasobow)
                                if(rldir=="web/"):
                                    body+="<p><a href=\""+request_uri+file+"\">"+file+"</a></p>\r\n"
                                else:
                                    body+="<p><a href=\""+request_uri+"/"+file+"\">"+file+"</a></p>\r\n"


                        #jesli jest to file to zwracamy damy plik z odpowiednim content typem
                        else:
                            #dostajemy rozszezenie pliku (do content typea)
                            mod=False
                            extension=os.path.splitext(dir+request_uri)[1]
                            #print(extension)
                            naglowek_status="HTTP/1.1 200 OK\r\n"
                            #print("file exists!")
                            if(extension==".txt"):
                                naglowek_typTresci="Content-Type: text/plain\r\n"
                                file=open(dir+request_uri,'rb')

                                body=str(file.read())
                                file.close()
                            if(extension==".html"):
                                naglowek_typTresci="Content-Type: text/html; charset=UTF-8\r\n"
                                file=open(dir+request_uri,'rb')
                                #print(file.readlines())
                                body=str(file.read())
                                file.close()
                            if(extension==".png"):
                                naglowek_typTresci="Content-Type: image/png; charset=UTF-8\r\n"
                                file=open(dir+request_uri,"rb")
                                body=file.read()
                                file.close()
                            if(extension==".jpg"):
                                naglowek_typTresci="Content-Type: image/jpg; charset=UTF-8\r\n"
                                file=open(dir+request_uri,"rb")
                                body=file.read()
                                file.close()



                    else:
                        naglowek_status="HTTP/1.1 404 File not found\r\n"
                        naglowek_typTresci="Content-Type: text/html; charset=UTF-8\r\n"
                        body="Error 404 File not found!"
                else:
                    naglowek_status="HTTP/1.1 400 Bad Request\r\n"
                    naglowek_typTresci="Content-Type: text/html; charset=UTF-8\r\n"
                    body="To nie jest GET!(lub HTTP)"





                header_date ="GMT-Date: "+email.utils.formatdate(usegmt=True)+"\r\n"
                #print(naglowek_typTresci)
                header=naglowek_status+naglowek_typTresci+header_date
                if(mod):
                    connection.sendall(header.encode('utf-8')+"\r\n".encode('utf-8')+body.encode('utf-8'))
                else:
                    connection.sendall(header.encode('utf-8')+"\r\n".encode('utf-8')+body)


        finally:
            # Zamkniecie polaczenia
            connection.close()


server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
#server_address = ('194.29.175.240', 31019)  # TODO: zmienić port!
server_address = ('127.0.0.1', 31019)  # TODO: zmienić port!
server_socket.bind(server_address)
while True:

    try:
        server_socket.listen(4)


    finally:

        http_serve(server_socket)

